import { Component, OnInit, Inject } from '@angular/core';

import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service';

import { flyInOut, expand } from '../animations/app.animations';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  host: {
    //mi assicura che l'animazione viene eseguita quando entro ed esco da MenuComponent
    '[@flyInOut]': 'true',
    'style': 'display: block;'
  },
  animations: [
    flyInOut(),
    expand()
  ]
})
export class MenuComponent implements OnInit {

  dishes: Dish[];
  errMess: string;

  //selectedDish: Dish;
  
  constructor(private dishService: DishService,
  @Inject('BaseURL') private BaseURL ) { }

  ngOnInit() {
    this.dishService.getDishes()
      .subscribe(dishes => this.dishes = dishes, //in caso di successo
      errmess => this.errMess = <any>errmess ); //in caso di errore
  }

  /*onSelect(dish : Dish) {
    this.selectedDish = dish;
  }*/

}
