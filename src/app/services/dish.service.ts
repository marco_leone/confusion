import { Injectable } from '@angular/core';
import { Dish } from '../shared/dish';
//import { DISHES } from '../shared/dishes';
import { Http, Response } from '@angular/http';

import { Observable } from 'rxjs';

import { baseURL } from '../shared/baseurl';
import { ProcessHTTPMsgService } from './process-httpmsg.service';

import { delay } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { RestangularModule, Restangular } from 'ngx-restangular';

@Injectable({
  providedIn: 'root'
})
export class DishService {

  constructor(private restangular: Restangular,
    private processHTTPMsgService: ProcessHTTPMsgService) { }
  
  getDishes(): Observable<Dish[]> {
    //return of(DISHES).pipe(delay(2000));
    /*return this.http.get(baseURL + 'dishes')
      .map(res => { return this.processHTTPMsgService.extractData(res)})
      .catch(error => {return this.processHTTPMsgService.handleError(error)});*/

    return this.restangular.all('dishes').getList(); //fetch ALL items from localhost:3000/dishes
  }

  getDish(id: number): Observable<Dish> {
    //return of(DISHES.filter( (dish) => (dish.id === id) )[0]).pipe(delay(2000));
    /*return this.http.get(baseURL + 'dishes/' + id)
      .map(res => { return this.processHTTPMsgService.extractData(res)})
      .catch(error => {return this.processHTTPMsgService.handleError(error)});*/
    return this.restangular.one('dishes', id).get(); //fetch ONE item from localhost/dishes/id
  }

  getFeaturedDish() : Observable<Dish> {
    //simulate server latency with 2 second delay
    //return of(DISHES.filter ( (dish) => (dish.featured))[0]).pipe(delay(2000));
    /*return this.http.get(baseURL + 'dishes?featured=true')
      .map(res => { return this.processHTTPMsgService.extractData(res)[0]})
      .catch(error => {return this.processHTTPMsgService.handleError(error)});*/
    return this.restangular.all('dishes').getList({featured: true}) //fetch all items with featured == true
      .map(dishes => dishes[0]); //return only first item of the list
  }

  getDishIds(): Observable<number[]> {
    //return of(DISHES.map(dish => dish.id)).pipe(delay(2000));
    return this.getDishes()
      .map(dishes => { return dishes.map(dish => dish.id )})
      .catch(error => {return this.processHTTPMsgService.handleError(error)} );
  }
}
