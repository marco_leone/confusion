import { Injectable } from '@angular/core';
import { Leader } from '../shared/leader';
//import { LEADERS } from '../shared/leaders';

import { Observable } from 'rxjs';
//import { delay } from 'rxjs/operators';
//import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { Http, Response } from '@angular/http';
import { baseURL } from '../shared/baseurl';
import { ProcessHTTPMsgService } from './process-httpmsg.service';

@Injectable({
  providedIn: 'root'
})
export class LeaderService {

  constructor(private http: Http,
    private processHTTPMsgService: ProcessHTTPMsgService) { }

  getLeaders(): Observable<Leader[]> {
    //return of(LEADERS).pipe(delay(2000));
    return this.http.get(baseURL + 'leaders')
      .map(res => { return this.processHTTPMsgService.extractData(res)})
      .catch(error => {return this.processHTTPMsgService.handleError(error)});
  }

  getLeader(id: number): Observable<Leader> {
    //return of(LEADERS.filter( (leader) => (leader.id === id))[0]).pipe(delay(2000));
    return this.http.get(baseURL + 'leaders/' + id)
      .map(res => { return this.processHTTPMsgService.extractData(res)})
      .catch(error => {return this.processHTTPMsgService.handleError(error)});
  }
 
  getFeaturedLeader(): Observable<Leader> {
    //return of(LEADERS.filter( (leader) => (leader.featured) )[0]).pipe(delay(2000));
    return this.http.get(baseURL + 'leaders?featured=true')
      .map(res => { return this.processHTTPMsgService.extractData(res)[0]})
      .catch(error => {return this.processHTTPMsgService.handleError(error)});
  }
}
