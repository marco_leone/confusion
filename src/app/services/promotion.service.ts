import { Injectable } from '@angular/core';

import { Promotion } from '../shared/promotion';
//import { PROMOTIONS } from '../shared/promotions';

import { Observable } from 'rxjs';
//import { delay } from 'rxjs/operators';
//import { of } from 'rxjs/observable/of';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { Http, Response } from '@angular/http';
import { baseURL } from '../shared/baseurl';
import { ProcessHTTPMsgService } from './process-httpmsg.service';


@Injectable({
  providedIn: 'root'
})
export class PromotionService {

  constructor(private http: Http,
    private processHTTPMsgService: ProcessHTTPMsgService) { }

  getPromotions(): Observable<Promotion[]> {
    //return of(PROMOTIONS).pipe(delay(2000));
    return this.http.get(baseURL + 'promotions')
      .map(res => { return this.processHTTPMsgService.extractData(res)})
      .catch(error => {return this.processHTTPMsgService.handleError(error)});
  }

  getPromotion(id: number): Observable<Promotion> {
    //return of(PROMOTIONS.filter( (promo) => (promo.id === id) )[0]).pipe(delay(2000));
    return this.http.get(baseURL + 'promotions/' + id)
      .map(res => { return this.processHTTPMsgService.extractData(res)})
      .catch(error => {return this.processHTTPMsgService.handleError(error)});
  }

  getFeaturedPromotion() : Observable<Promotion> {
    //return of(PROMOTIONS.filter ( (promo) => (promo.featured))[0]).pipe(delay(2000));
    return this.http.get(baseURL + 'promotions?featured=true')
      .map(res => { return this.processHTTPMsgService.extractData(res)[0]})
      .catch(error => {return this.processHTTPMsgService.handleError(error)});
  }
}
