import { Component, OnInit, Inject } from '@angular/core';
import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { visibility, flyInOut, expand } from '../animations/app.animations';

import 'rxjs/add/operator/switchMap';

 
@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  host: {
    //mi assicura che l'animazione viene eseguita quando entro ed esco da MenuComponent
    '[@flyInOut]': 'true',
    'style': 'display: block;'
  },
  animations: [
    flyInOut(),
    visibility(),
    expand()
  ]
})


export class DishdetailComponent implements OnInit {

  //@Input - per ricevere parametri di input da template html (?)

  dish: Dish;
  dishcopy = null; //usato per memorizzare un dish nella forma di Restangular object ottenuto dal server;
  dishIds: number[];
  prev: number;
  next: number;
  commentForm: FormGroup;
  errMess: string;
  visibility = 'shown';

  formErrors = {
    'author': '',
    'comment': ''
  };

  validationMessages = {
    'author': {
      'required': 'First name is required',
      'minlength': 'First name must be at least 2 characters long',
      'maxlength': 'First name cannot be more than 25 characters long'
    },
    'comment': {
      'required': 'Last name is required',
      'minlength': 'Last name must be at least 2 characters long',
      'maxlength': 'Last name cannot be more than 25 characters long'
    }
  }


  constructor(private dishservice: DishService,
    private route : ActivatedRoute,
    private location :Location,
    private fb: FormBuilder,
    @Inject('BaseURL') private BaseURL ) { 
      this.createForm();

      this.commentForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); 

    }

  
  createForm() {
      this.commentForm = this.fb.group({
        rating: [5, [Validators.required, Validators.pattern] ],
        comment: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(1000)]],
        author: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)] ]
      });
  }

  onValueChanged(data? : any) {
    if(!this.commentForm) { return; }
    const form = this.commentForm;

    for(const field in this.formErrors) {
      this.formErrors[field] = ''; //resetto errore
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  onSubmit() {
    console.log(this.commentForm.value);
    let comment = this.commentForm.value;
    comment.date = new Date().toISOString();
    this.dishcopy.comments.push(comment);
    this.dishcopy.save() //modifiche fatte a dishcopy sono rese persistenti lato server
      .subscribe(dish => this.dish = dish); //viene restituito un observable contenente una copia del Dish per cui viene fatta subscribe; il dish restituito viene quindi salvato client-side
    this.commentForm.reset({
      author: '',
      comment: '',
      rating: 5
    });
  }

  ngOnInit() {
    //this.createForm();

    this.dishservice.getDishIds()
    .subscribe(dishIds => this.dishIds = dishIds);

    //+params['id']: converte params['id'], che è una stringa, in un number
    //ogni volta che l'Observable route.params cambia, viene invocato il metodo getDish che restituisce un Dish;
    //di conseguenza ad ogni cambiametno di route.params viene aggiornato anche il valore della variabile dish
    this.route.params
      //quando inizio il fetch di un nuovo Dish, al variare di un parametro del route, setto la visibility a Hidden
      .switchMap((params: Params) => {this.visibility = 'hidden'; return this.dishservice.getDish(+params['id'])} )
      .subscribe(dish => {
        this.dish = dish;
        this.dishcopy = dish;
        this.setPrevNext(dish.id);
        this.visibility = 'shown' //quando il nuovo Dish diventa disponibile, setto la visibility a Shown
      }, 
      errmess => this.errMess = <any>errmess);
  }



  setPrevNext(dishId: number) {
    let index = this.dishIds.indexOf(dishId);
    //se dishId = 0, prev è l'ultimo elemento dell'array (una sorta di visita circolare dell'array)
    this.prev = this.dishIds[(this.dishIds.length + index - 1)%this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1)%this.dishIds.length];

  }

  goBack(): void {
    this.location.back();
  }

}
